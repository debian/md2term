# md2term

Markdown parser for highlights and colors in terminal.

![](screenshot/image.png)

## Objectives

`md2term` was created to be a markdown parser for short texts (less than 30 lines) that could be used in terminal presentations (*text slides*) and, at the same time, able to be correctly visualized by other parsers on websites (GitHub, GitLab, etc). The script can also be used satisfactorily to view other texts in markdown, as long as its [limitations](#limita%C3%A7%C3%B5es) are observed.


## Usage

```
md2term [OPTIONS] -f FILE

OPTIONS:

    -b CHARACTER     List marker character.

    -c               Clears the terminal before displaying text.

    -i SPACES        Indentation of lists, codes and citations in spaces.

    -k VALUE         Keep hash marks in titles:
                     0=Discard all 1=Keep all 2=Only in title 1

    -m SPACES        Ident from left margin in spaces.

    -p               Load a pager (less).

    -s FILE          Alternative color scheme.

    -t LINES         Top offset in lines.

    -h               Show this help.
```

## Dependencies

- Bash
- [Pandoc](https://pandoc.org/)

## Installation

Clone the repository:

```
git clone https://codeberg.org/blau_araujo/md2term
```

Enter the `md2term` directory that was created and run `make`:

```
cd /path/to/md2term
sudo make install
```

Uninstalling:

```
cd /path/to/md2term
sudo make uninstall
```

## Valid markings

| Marking | Output |
|---|---|
| `**TEXT**` or `__TEXT__` | BOLD |
| `*TEXT*` or `*TEXT*` | ITALIC |
| `___TEXT___` or `***TEXT***` | BOLD AND ITALIC |
| `~~TEXT~~` | STRIKETHROUGH |
| `<u>TEXT</u>` | UNDERLINE |
| ``` `TEXT` ``` | CODE IN LINE |
| `[LABEL](URL)` | LABEL |

### BLOCKS

#### Code block

~~~
```
CODE BLOCK
```
~~~

#### Unordered lists

```
- Item 1
- Item 2
- Item 3
```

#### Ordered lists

```
1. Item 1
1. Item 2
1. Item 3
```

#### Quotes/notes

```
> The text of the quote or note.
```

#### Force line break

```
Paragraphs, list items, and quotes can \
have lines broken where desired \
using backslashes.
```

## LIMITATIONS

- Does not interpret nested lists (sub-items) correctly.
- Long line breaks can happen in the middle of words, but you can work around the problem by specifying where they should happen in the source text (this is a valid GFM markdown markup).
- Using the `-c` and `-p` options together can lead to buffer redraw problems in `less`.
- Links are just visual representations using the markup label (not the URL). If you prefer the URL, it can be used as a label or written as plain text (most terminals interpret URLs as links).

## LICENSE

Copyright (C) 2022 Blau Araujo <blau@debxp.org>

GPLv3+ License: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html> \
This is free software: you are free to change and redistribute it. \
THERE IS NO WARRANTY, to the fullest extent permitted by law.

Developed by Blau Araujo and Romeu Alfa
