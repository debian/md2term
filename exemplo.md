# TÍTULO DA APRESENTAÇÃO

Este é um **teste** dos `slides` em *markdown*. Aqui temos textos em **negrito**, \
em *itálico*, textos <u>sublinhados</u> ou ~~cortados~~, além de uma combinação \
do ***negrito com o itálico*** e da simulação de um [link](url).


## AQUI TEMOS UM SUBTÍTULO

Também \n interpretamos `código inline` e blocos de código:

```
for n in $teste; do
    echo $n
done
```

Os blocos de código podem ser usados para demonrtrar marcações:

```
LINK: [label](url)
```

### AGORA É UM SUB-SUBTÍTULO!

Legal, não é?  Que tal ver uma lista não ordenada?

- O texto fonte será renderizado corretamente em qualquer plataforma \
  que interprete marcações compatíveis com o GitHub markdown. Os itens \
  das listas podem receber **estilos**! 

- Como **negrito**, *itálico*, e <u>sublinhado</u>.

- Além dos `códigos` e dos [links simulados](url)!

E uma lista ordenada?

1. Item um
1. Item dois
1. Item três


## OLHA A CITAÇÃO AÍ!

> **Importante:** Existem limitações, afinal, este parser tem como objetivo \
exibir destaques e cores no terminal. A principal vantagem, porém, é \
que o texto fonte será renderizado corretamente em qualquer plataforma \
que interprete marcações compatíveis com o GitHub markdown.

***That's all, folks!***














